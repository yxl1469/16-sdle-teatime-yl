{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align=\"center\">Tea Time Python: SciPy Topics</h1>\n",
    "License: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)  \n",
    "The previous three notebooks have given us the tools we need to explore more specific and quite possibly more useful topics. In this notebook we will cover:\n",
    "* Interpolation\n",
    "* Numerical Derivatives\n",
    "* Numerical Integrals\n",
    "* Linear Algebra\n",
    "* Fast Fourier Transform (FFT)\n",
    "\n",
    "Many of these topics have courses built around them; this notebook is not intended to teach these topics. The purpose is to show how to use python (SciPy) to solve various problems in these areas. \n",
    "\n",
    "There are also a couple other SciPy topics that require more than simply calling the relevant function and may require you to build your own function that fit the routines in python. They require a bit more effort conceptually and in volume of code to execute in python. These topics are:\n",
    "* Ordinary Differential Equations (ODE)\n",
    "* Fitting/Optimization\n",
    "\n",
    "They will not be covered in Tea Time unless I find that there is additional time left over. However, links to examples have been included in this notebook.\n",
    "\n",
    "Solving problems related to all of these topics in python has quite a bit of value. One analogy is that python is like using a calculator to solve basic math problems: it makes things easier by solving problems for you as it often only requires the input. An extension to this concept is that SciPy is pretty good at processing larger volumes of data using arrays. However, there is more value than just having the code do all the work. There are a number of problems that cannot be solved analytically, but can be solved numerically by being a little bit creative. An example of this, which we will see shortly, is a function that cannot be inverted analytically. However, using an interpolating spline, it becomes possible to numerically invert the function.\n",
    "\n",
    "A good resource for all of these topics is the course web page for Physics 250. I learned python taking this course and often use the page for reference. There are examples for many of these topics. If you cannot find what you are looking for in the examples, the prelabs and labs also have solutions and often go more in depth and give more specific examples.  \n",
    "http://www.phys.cwru.edu/courses/p250/\n",
    "\n",
    "Let's start by loading everything we will need. We have a wide array of topics, so we will need a number of tools. As I may have mentioned earlier, there is significant overlap between SciPy and numpy for some of these topics. For most of them we will stick to SciPy because it more tools available for each of them. For the FFT, we will use numpy.fft as opposed to scipy.signal simply because it has everything we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib\n",
    "%matplotlib inline\n",
    "import scipy.interpolate as interp\n",
    "import scipy.integrate as integ\n",
    "import scipy.optimize as opt\n",
    "import scipy.linalg as la\n",
    "import scipy.special as sf\n",
    "matplotlib.rc('font', size = 15)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interpolation and Derivatives\n",
    "There are a number of options in the interpolate module. For one dimension there are three options that we will look into.\n",
    "\n",
    "The first is a polynomial spline. **interp.lagrange** is good for small numbers of points. However, it is important to pay attention to the documentation. This option is not good for any more than 20 points.\n",
    ">Warning: This implementation is numerically unstable. Do not expect to\n",
    ">be able to use more than about 20 points even if they are chosen optimally.\n",
    "\n",
    "The other two options are **interp.interp1d** and **interp.InterpolatedUnivariateSpline**. Both have their uses and limits. The biggest difference is that **interp1d** does not allow for extrapolation, which is often a good thing because extrapolation can be dangerous. The x input for **InterpolatedUnivariateSpline**  must be ascending or it will return a less than desireable spline.\n",
    "\n",
    "Please note that making a spline involves creating a function class attribute. This function is the spline, in which you can input your x data and it will return the values for y."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "interp?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "interp.lagrange?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "interp.interp1d?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "interp.InterpolatedUnivariateSpline?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def yfunct(x):\n",
    "    y = .5 * np.sin(x) + .5 * np.sin(x/2)\n",
    "    return(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Make a x data for high resolved function and spline\n",
    "n = 1000\n",
    "x = np.linspace(0,4*np.pi,n)\n",
    "y = yfunct(x)\n",
    "\n",
    "#select some random points in this curve to use for interopolation\n",
    "splinepoints = 15\n",
    "xsplinept = x[np.random.randint(n, size = splinepoints)]\n",
    "xsplinept.sort()\n",
    "ysplinept = yfunct(xsplinept)\n",
    "\n",
    "polyspline = interp.lagrange(xsplinept, ysplinept)\n",
    "cubicspline = interp.InterpolatedUnivariateSpline(xsplinept, ysplinept)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 6))\n",
    "\n",
    "plt.plot(x, y, linewidth = 2.5, label=\"True Curve\")\n",
    "plt.plot(xsplinept, ysplinept, 'o', label=\"Random Points for Interpolation\")\n",
    "plt.plot(x, polyspline(x), '--', linewidth = 2.5, label=\"Polynomial\")\n",
    "plt.plot(x, cubicspline(x), 'm-.', linewidth = 2.5, label=\"Cubic Spline\")\n",
    "plt.legend(loc = \"best\")\n",
    "plt.xlabel('x values')\n",
    "plt.ylabel('y values')\n",
    "plt.title('Spline Demo')\n",
    "plt.minorticks_on()\n",
    "plt.xlim(0,4*np.pi)\n",
    "plt.ylim(-1.2,1.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a number of ways to take the derivative of your function or data. Some ways are better than others, and some will take too long to teach in Tea Time. For more details, see the Physics 250 web page. Since we already know how to make a spline, taking the derviative from here is actually very easy. Also, using a spline to find the derivative reduces the impact of noise amplification when taking the derivative of noisy data. Please note that there are also smoothing options for the splines that we will not be covering. \n",
    "\n",
    "From the data that we already have, taking the derivative is simple. We just need to pass the spline function that we created a second argument: the degree of the derivative. It is a good idea to give the spline as much data to work with as possible, so we will increase the number of points that we use to  define our spline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Get more points for making the spline\n",
    "splinepoints = 30\n",
    "xsplinept = x[np.random.randint(n, size = splinepoints)]\n",
    "xsplinept.sort()\n",
    "ysplinept = yfunct(xsplinept)\n",
    "\n",
    "cubicspline = interp.InterpolatedUnivariateSpline(xsplinept, ysplinept)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 6))\n",
    "\n",
    "plt.plot(x, y, linewidth = 2.5, label=\"True Curve\")\n",
    "#plt.plot(xsplinept, ysplinept, 'o', label=\"Random Points for Interpolation\")\n",
    "plt.plot(x, cubicspline(x), 'r-.', linewidth = 2.5, label=\"Cubic Spline\")\n",
    "plt.plot(x, cubicspline(x,1), 'm-.', linewidth = 2.5, label=\"First Deriviative Spline\")\n",
    "#plt.plot(x, cubicspline(x,2), 'y-.', linewidth = 2.5, label=\"Second Deriviative Spline\")\n",
    "plt.legend(loc = \"best\")\n",
    "plt.xlabel('x values')\n",
    "plt.ylabel('y values')\n",
    "plt.title('Spline Demo')\n",
    "plt.minorticks_on()\n",
    "plt.xlim(0,4*np.pi)\n",
    "plt.ylim(-1.2,1.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Inverting a function is relatively simple. We simply pass the the points to our chosen interpolating spline in the reverse order. Please note that the y values must be ascending in this case, so reversing the order may be necessary. In the example, both are increasing so this is not necessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#Create data for a function to invert\n",
    "x = np.linspace(0,2.5,500)\n",
    "y = x**3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "invspline = interp.InterpolatedUnivariateSpline(y,x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 6))\n",
    "plt.plot(x, x**(1./3), linewidth = 2.5, label=\"$y = \\sqrt[3]{x}$\")\n",
    "plt.plot(y, invspline(y), '--', linewidth = 2.5, label=\"Spline Inverse of $y=x^3$\")\n",
    "plt.legend(loc = \"best\")\n",
    "plt.xlabel('x values')\n",
    "plt.ylabel('y values')\n",
    "plt.title('Sample Function to Invert')\n",
    "plt.minorticks_on()\n",
    "plt.xlim(0,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numeric Integrals\n",
    "\n",
    "There are a number of methods for integrating numeric data. For the sake of time we will not discuss them in detail and just skip to work horse function in scipy.integrate. This function is **integ.quad**, which is a general tool that utilizes a number of techniques. You must pass it three arguments: the functions to integrate (this can be a spline if your data does not have a function), the lower limit, and the upper limit.  It will return the value of the integral and an error, which should be very close to zero. Note that this is just the error of the algorithm used by quad, not your true error.\n",
    "\n",
    "Using this function may require loops."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "integ.quad?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "integ.quad(np.cos,0,np.pi/2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#integrate over a cosine function from zero to pi\n",
    "x = np.linspace(0, np.pi, 100)\n",
    "y = np.zeros(len(x))\n",
    "for i in range(len(x)):\n",
    "    y[i] = integ.quad(np.cos,0,x[i])[0]\n",
    "\n",
    "#plot to see the result!    \n",
    "plt.plot(x,y, linewidth = 2.5)\n",
    "#plt.plot(x,np.sin(x), 'r--', linewidth = 2.5)    \n",
    "plt.xlabel('x values')\n",
    "plt.ylabel('y values')\n",
    "plt.title('Integrating np.cos')\n",
    "plt.minorticks_on()\n",
    "plt.xlim(0,np.pi);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ordinary Differential Equations (ODE)\n",
    "http://nbviewer.jupyter.org/url/www.phys.cwru.edu/courses/p250/examples/ODE.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "integ.odeint?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting\n",
    "http://nbviewer.jupyter.org/url/www.phys.cwru.edu/courses/p250/examples/Chisq.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "opt.curve_fit?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Algebra\n",
    "\n",
    "I may have mentioned earlier that a 2-dimensional array can be thought of almost as a matrix. This section will explore the basic tools and functions used by python for linear algebra. A common topic in linear algebra is systems of linear equations. Any system of linear equations can be formatted to use matrices. All the coefficients go into a matrix **A**, the variables go into column vector **x** and the solutions are in a column vector **B**. For example:\n",
    "$$ x_1 + -2*x_2 + 3*x_3 = 7$$  \n",
    "$$ 2*x_1 + x_2 + x_3 = 4$$  \n",
    "$$ -3*x_1 + 2*x_2 + -2*x_3 = -10$$  \n",
    "will result in \n",
    "$$A = \\begin{bmatrix}\n",
    "    1 & -2 & 3 \\\\\n",
    "    2 & 1 & 1 \\\\\n",
    "    -3 & 2 & -2 \\\\\n",
    "\\end{bmatrix}\n",
    "x = \\begin{bmatrix}\n",
    "    x_1 \\\\\n",
    "    x_2 \\\\\n",
    "    x_3 \\\\\n",
    "\\end{bmatrix}\n",
    "B = \\begin{bmatrix}\n",
    "    7 \\\\\n",
    "    4 \\\\\n",
    "    -10 \\\\\n",
    "\\end{bmatrix}$$\n",
    "The Matrices form the equation **Ax**=**B**. This is easily solved for **x** using **la.solve**. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "A = np.array([[1.,-2,3],[2,1,1],[-3,2,-2]])\n",
    "B = np.array([7.,4,-10])\n",
    "x = la.solve(A,B)\n",
    "print x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's verify this solution by checking that **A** **x** = **B**. To multiply arrays as matrices, we do not use \\*. That will multiply the components. We use **np.dot** for matrix multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.dot(A,x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some other useful shortcuts that we do not already know how to do easily with our current knowledge of array operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Transpose\n",
    "print A.T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Identity matrix- nxn\n",
    "print np.eye(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Determinant\n",
    "print la.det(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Diagonalize a 1-D array\n",
    "print np.diag(B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PLU factorization is also made easy. \n",
    "\n",
    "**la.lu_factor** and **la.lu_solve** can be used to make a large number of calculations easier on the memory than **la.solve**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "P,L,U = la.lu(A)\n",
    "print \"Permutation:\\n\", P\n",
    "print \"Lower Triangular:\\n\", L\n",
    "print \"Upper Triangular:\\n\", U"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, eigenvalues and eigenvectors can be found with a single function, **la.eig**. This uses the notation $j = \\sqrt{-1}$ for complex numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "values, vectors = la.eig(A)\n",
    "print \"Eigenvalues:\\n\", values\n",
    "print \"Matrix of Eigenvectors\\n\", vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last Linear Algebra topic that we will briefly cover is SVD. Singular Value Decomposition (SVD) has a handful of useful functions related to it. The function **la.svd** can be used for find the matrices for SVD, but there are a few other relevant functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Simply get the values\n",
    "print \"Singular Values:\", la.svdvals(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Pseudo Inverse\n",
    "print \"Pseudo Inverse\\n\", la.pinv(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Pseudo Inverse found using SVD\n",
    "print la.pinv2(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fast Fourier Transform (FFT)\n",
    "Let's do a quick demo of the FFT functions. This doesn't get interesting without noise, so we should add some noise to our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "npoints = 10001\n",
    "t = np.linspace(0,24*np.pi,npoints)\n",
    "A = 10*yfunct(t)\n",
    "noise = np.random.randn(npoints)\n",
    "An = A + 20*noise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 6))\n",
    "\n",
    "plt.plot(t, An, 'r.', label=\"Noisy signal\")\n",
    "plt.plot(t, A, linewidth = 2.5, label=\"True Signal\")\n",
    "plt.legend(loc = \"best\")\n",
    "plt.xlabel('Time')\n",
    "plt.ylabel('Amplitude')\n",
    "plt.title('Noisy Signal')\n",
    "plt.minorticks_on()\n",
    "plt.xlim(0,24*np.pi);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The functions that we will use are **np.fft.fft** and **np.fft.fftfreq**. The frequencies for the values returned in the fft are stored in **np.fft.fftfreq**. The frequencies are returned starting at f=0 (DC) and then listed in ascending order, followed by descending order for negative values. We can then use our knowledge of array indexing to pull out the positive and negative values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "fft = np.fft.fft(An)\n",
    "freq = np.fft.fftfreq(npoints)\n",
    "print \"Frequencies returned:\", freq\n",
    "i_freq = np.arange(1,npoints//2)\n",
    "pfreq = freq[i_freq]\n",
    "nfreq = freq[-i_freq]\n",
    "print \"Positive frequencies:\",freq[i_freq]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From here it is easy to find the Power Spectral Density (PSD) to visually look at the dominant frequencies in our data. We expect to see them for wavelenths of 2$\\pi$ and 4$\\pi$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "psd = np.abs(fft[i_freq])**2 + np.abs(fft[-i_freq])**2\n",
    "plt.plot(pfreq*(n), psd, 'k-')\n",
    "plt.xlim(0,5);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "peaks, = np.where(psd > 1e9)\n",
    "peakfreq = pfreq[peaks]\n",
    "wave = 1/peakfreq\n",
    "print wave * 24/npoints"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
